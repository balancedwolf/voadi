from PIL import Image
import os, sys


destination_folder = sys.argv[1] 
source_height = int(sys.argv[2])
target_height = int(sys.argv[3])

ratio = target_height / source_height

path = destination_folder


def resize(destination):
    for root, subdirs, files in os.walk(destination):
        for file in files:
            if file.endswith('png'):
                im = Image.open(os.path.join(root, file))
                f, e = os.path.splitext(os.path.join(root, file))
                size = im.size
                final_width = int(size[0] * ratio)
                final_height = int(size[1] * ratio)
                imResize = im.resize((final_width,final_height), Image.NEAREST)
                imResize.save(f + '.png', 'png', quality=100, optimize=True)

resize(path + "/" + "sprites")
resize(path + "/" + "tilesets")
