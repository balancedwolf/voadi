-- luarocks install penlight
local prettyprint = require('pl.pretty')

function resize_position_table(data, ratio)
	data['x'] 		= math.ceil(data['x'] * ratio)		
	data['y'] 		= math.ceil(data['y'] * ratio)		

	return data
end

function resize_size_table(data, ratio)
	data['width'] 		= math.ceil(data['width'] * ratio)		
	data['height'] 		= math.ceil(data['height'] * ratio)		

	return data
end

function resize_size_width_table(data, ratio)
	data['width'] 		= math.ceil(data['width'] * ratio)		
	return data
end

function resize_size_height_table(data, ratio)
	data['height'] 		= math.ceil(data['height'] * ratio)		
	return data
end

function resize_map(filename, output, ratio)
	local map_properties = {}
	local map_tiles = {}
	local map_dynamic_tiles = {}
	local map_teletransporters = {}
	local map_destinations = {}
	local map_pickables = {}
	local map_destructibles = {}
	local map_chests = {}
	local map_shop_items = {}
	local map_enemies = {}
	local map_npcs = {}
	local map_blocks = {}
	local map_jumpers = {}
	local map_switches = {}
	local map_sensors = {}
	local map_walls = {}
	local map_crystals = {}
	local map_crystal_blocks = {}
	local map_streams = {}
	local map_doors = {}
	local map_stairs = {}
	local map_separators = {}
	local map_custom_entities = {}

	function properties(data)
		data = resize_position_table(data, ratio)
		data = resize_size_table(data, ratio)
		map_properties = data
	end

	function tile(data)
		data = resize_position_table(data, ratio)
		data = resize_size_table(data, ratio)
		map_tiles[#map_tiles + 1] = data
	end

	function dynamic_tile(data)
		data = resize_position_table(data, ratio)
		data = resize_size_table(data, ratio)
		map_dynamic_tiles[#map_dynamic_tiles + 1] = data
	end

	function teletransporter(data)
		data = resize_position_table(data, ratio)
		data = resize_size_table(data, ratio)
		map_teletransporters[#map_teletransporters + 1] = data
	end

	function destination(data)
		data = resize_position_table(data, ratio)
		data["y"] = data["y"] - (8 * ratio)
		map_destinations[#map_destinations + 1] = data
	end

	function pickable(data)
		data = resize_position_table(data, ratio)
		map_pickables[#map_pickables + 1] = data
	end

	function destructible(data)
		data = resize_position_table(data, ratio)
		map_destructibles[#map_destructibles + 1] = data
	end

	function chest(data)
		data = resize_position_table(data, ratio)
		map_chests[#map_chests + 1] = data
	end

	function shop_item(data)
		data = resize_position_table(data, ratio)
		map_shop_items[#map_shop_items + 1] = data
	end

	function enemy(data)
		data = resize_position_table(data, ratio)
		map_enemies[#map_enemies + 1] = data
	end

	function npc(data)
		data = resize_position_table(data, ratio)
		map_npcs[#map_npcs + 1] = data
	end

	function block(data)
		data = resize_position_table(data, ratio)
		map_blocks[#map_blocks + 1] = data
	end

	function jumper(data)
		data = resize_position_table(data, ratio)
		local direction = tonumber(data["direction"])

		if direction == 2 or direction == 6 then
			data = resize_size_width_table(data, ratio)
		elseif direction == 0 or direction == 4 then
			data = resize_size_height_table(data, ratio)
		else
			data = resize_size_table(data, ratio)
		end

		map_jumpers[#map_jumpers + 1] = data
	end

	function switch(data)
		data = resize_position_table(data, ratio)
		map_switches[#map_switches + 1] = data
	end

	function sensor(data)
		data = resize_position_table(data, ratio)
		data = resize_size_table(data, ratio)
		map_sensors[#map_sensors + 1] = data
	end

	function wall(data)
		data = resize_position_table(data, ratio)
		data = resize_size_table(data, ratio)
		map_walls[#map_walls + 1] = data
	end

	function crystal(data)
		data = resize_position_table(data, ratio)
		map_crystals[#map_crystals + 1] = data
	end

	function crystal_block(data)
		data = resize_position_table(data, ratio)
		data = resize_size_table(data, ratio)
		map_crystal_blocks[#map_crystal_blocks + 1] = data
	end

	function stream(data)
		data = resize_position_table(data, ratio)
		map_streams[#map_streams + 1] = data
	end

	function door(data)
		data = resize_position_table(data, ratio)
		map_doors[#map_doors + 1] = data
	end

	function stairs(data)
		data = resize_position_table(data, ratio)
		map_stairs[#map_stairs + 1] = data
	end

	function separator(data)
		data = resize_position_table(data, ratio)
		if tonumber(data["width"]) == 16 then
			data = resize_size_height_table(data, ratio)
		elseif tonumber(data["height"]) == 16 then
			data = resize_size_width_table(data, ratio)
		end


		map_separators[#map_separators + 1] = data
	end

	function custom_entity(data)
		data = resize_position_table(data, ratio)
		data = resize_size_table(data, ratio)
		map_custom_entities[#map_custom_entities + 1] = data
	end

	local env = setmetatable({}, {__index=_G})

	loadfile(
	   filename,
	   "t",
	   env
	)()



	local file,err = io.open(output,'w')
	if file then
		file:write("properties" .. prettyprint.write(map_properties) .. "\n")

		for i = 1, #map_tiles do
			file:write("tile" .. prettyprint.write(map_tiles[i]) .. "\n")
		end

		for i = 1, #map_dynamic_tiles do
			file:write("dynamic_tile" .. prettyprint.write(map_dynamic_tiles[i]) .. "\n")
		end

		for i = 1, #map_teletransporters do
			file:write("teletransporter" .. prettyprint.write(map_teletransporters[i]) .. "\n")
		end


		for i = 1, #map_destinations do
			file:write("destination" .. prettyprint.write(map_destinations[i]) .. "\n")
		end


		for i = 1, #map_pickables do
			file:write("pickable" .. prettyprint.write(map_pickables[i]) .. "\n")
		end


		for i = 1, #map_destructibles do
			file:write("destructible" .. prettyprint.write(map_destructibles[i]) .. "\n")
		end

		for i = 1, #map_chests do
			file:write("chest" .. prettyprint.write(map_chests[i]) .. "\n")
		end

		for i = 1, #map_shop_items do
			file:write("shop_item" .. prettyprint.write(map_shop_items[i]) .. "\n")
		end

		for i = 1, #map_enemies do
			file:write("enemy" .. prettyprint.write(map_enemies[i]) .. "\n")
		end

		for i = 1, #map_npcs do
			file:write("npc" .. prettyprint.write(map_npcs[i]) .. "\n")
		end

		for i = 1, #map_blocks do
			file:write("block" .. prettyprint.write(map_blocks[i]) .. "\n")
		end

		for i = 1, #map_jumpers do
			file:write("jumper" .. prettyprint.write(map_jumpers[i]) .. "\n")
		end

		for i = 1, #map_switches do
			file:write("switch" .. prettyprint.write(map_switches[i]) .. "\n")
		end

		for i = 1, #map_sensors do
			file:write("sensor" .. prettyprint.write(map_sensors[i]) .. "\n")
		end

		for i = 1, #map_walls do
			file:write("wall" .. prettyprint.write(map_walls[i]) .. "\n")
		end

		for i = 1, #map_crystals do
			file:write("crystal" .. prettyprint.write(map_crystals[i]) .. "\n")
		end


		for i = 1, #map_crystal_blocks do
			file:write("crystal_block" .. prettyprint.write(map_crystal_blocks[i]) .. "\n")
		end

		for i = 1, #map_streams do
			file:write("stream" .. prettyprint.write(map_streams[i]) .. "\n")
		end

		for i = 1, #map_doors do
			file:write("door" .. prettyprint.write(map_doors[i]) .. "\n")
		end

		for i = 1, #map_stairs do
			file:write("stairs" .. prettyprint.write(map_stairs[i]) .. "\n")
		end

		for i = 1, #map_separators do
			file:write("separator" .. prettyprint.write(map_separators[i]) .. "\n")
		end

		for i = 1, #map_custom_entities do
			file:write("custom_entity" .. prettyprint.write(map_custom_entities[i]) .. "\n")
		end

	    file:close()
	else
	    print("error: ", err) 
	end
end


function resize_sprite(filename, output, ratio)
	local resized_animations = {}

	function animation(data)
		local directions = data['directions']


		for i = 1, #directions do
			local direction = {}

			local original_direction = directions[i]


			--for _,k in pairs(sortedKeys(original_direction, function(a, b) return a:upper() > b:upper() end)) do
			  --print(k, original_direction[k])
			--end

			--for k, v in ipairs(original_direction) do
				--table.insert(direction, {k, v})
				--print(v)
			--end
			original_direction['x'] 				= math.ceil(directions[i]['x'] * ratio)
			original_direction['y'] 				= math.ceil(directions[i]['y'] * ratio)
			original_direction['frame_width'] 	= math.ceil(directions[i]['frame_width'] * ratio)
			original_direction['frame_height'] 	= math.ceil(directions[i]['frame_height'] * ratio)
			original_direction['origin_x'] 		= math.ceil(directions[i]['origin_x'] * ratio)
			original_direction['origin_y'] 		= math.ceil(directions[i]['origin_y'] * ratio)

			local resized_direction = original_direction

			table.sort(resized_direction, function(a, b) return a:upper() > b:upper() end)		

			directions[i] = resized_direction
		end

		data['directions'] = directions

		resized_animations[#resized_animations + 1] = data
	end

	-- Create a new "env" table that will look up missing keys in the global environment
	local env = setmetatable({}, {__index=_G})

	loadfile(
	   filename, -- Your file
	   "t", -- Only read Lua source file, no bytecode
	   env -- Load the code with "env" as its environment
	)() -- Run the code right after loading it


	local file,err = io.open(output,'w')
	if file then
		for i = 1, #resized_animations do
			file:write("animation" .. prettyprint.write(resized_animations[i]) .. "\n")
		end
	    
	    file:close()
	else
	    print("error: ", err) 
	end
end


function resize_tileset(filename, output, ratio)
	local resized_tile_patterns = {}
	local tileset_background_color = {}

	function background_color(data)
		tileset_background_color = data
	end


	function tile_pattern(data)
		local original_tile_pattern = data

		if type(original_tile_pattern['x']) == "table" then
			for i, v in ipairs(original_tile_pattern['x']) do
				original_tile_pattern['x'][i] = original_tile_pattern['x'][i] * ratio
			end
		else
			original_tile_pattern['x'] 		= math.ceil(original_tile_pattern['x'] * ratio)		
		end
		
		if type(original_tile_pattern['x']) == "table" then
			for i, v in ipairs(original_tile_pattern['x']) do
				original_tile_pattern['y'][i] = original_tile_pattern['y'][i] * ratio
			end
		else
			original_tile_pattern['y'] 		= math.ceil(original_tile_pattern['y'] * ratio)
		end	
		
		original_tile_pattern['width'] 	= math.ceil(original_tile_pattern['width'] * ratio)
		original_tile_pattern['height'] = math.ceil(original_tile_pattern['height'] * ratio)

		local resized_tile_pattern = original_tile_pattern

		table.sort(resized_tile_pattern, function(a, b) return a:upper() > b:upper() end)		

		resized_tile_patterns[#resized_tile_patterns + 1] = data
	end


	loadfile(
	   filename, 
	   "t", 
	   env
	)() 


	local file,err = io.open(output,'w')
	if file then
		file:write("background_color" .. prettyprint.write(tileset_background_color) .. "\n")

		for i = 1, #resized_tile_patterns do
			file:write("tile_pattern" .. prettyprint.write(resized_tile_patterns[i]) .. "\n")
		end
	    
	    file:close()
	else
	    print("error: ", err) 
	end

end

local solarus_resizer = {}

solarus_resizer.resize_sprite = resize_sprite
solarus_resizer.resize_tileset = resize_tileset
solarus_resizer.resize_map = resize_map

return solarus_resizer