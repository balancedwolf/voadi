--  luarocks install LuaFileSystem
require("lfs")
local solarus_resizer = require("solarus_resizer_helper")

local origin = arg[1]
local destination = arg[2]

local original_size = arg[3]
local target_size = arg[4]

local ratio = target_size / original_size

function copy_folder(origin, destination)
	--local res, err = fs.copy(origin, destination .. "/" .. tostring(target_size) ..".over." .. tostring(original_size))
	print(fs.readdir(origin))
end

--copy_folder(origin, destination)

local target_directory = destination .. "/" .. tostring(target_size) ..".over." .. tostring(original_size)

lfs.mkdir(target_directory)

function copy_folder (path, prefix, destination)
	for file in lfs.dir(path) do
    	if file ~= "." and file ~= ".." then

       		local f = path..'/'..file

          	local attr = lfs.attributes (f)
          	assert (type(attr) == "table")
          	if attr.mode == "directory" then
          		local res, _ = string.gsub(f, "%" .. prefix, destination)
          		print(res)
          		lfs.mkdir(res)

            	copy_folder (f, prefix, destination)
          	else

          		local res, _ = string.gsub(path, "%" .. prefix, destination)
          		lfs.mkdir(res)


          		local d = res .. "/" .. file
				
          		print(d)

				local infile = io.open(f, "rb")
				local instr = infile:read("*a")
				infile:close()

				local outfile = io.open(d, "wb")
				outfile:write(instr)
				outfile:close()

        	end
    	end
	end
end


function process_maps (path, destination)
	for file in lfs.dir(path) do
    	if file ~= "." and file ~= ".." then

       		local f = path..'/'..file

          	local attr = lfs.attributes (f)
          	assert (type(attr) == "table")
          	if attr.mode == "directory" then
            	process_maps (f, destination)
          	else

          		if string.find(f, destination) then
          			local extension = f:match("^.+(%..+)$")
          			if extension == ".dat" then
          				solarus_resizer.resize_map(f, f, ratio)
      				end
      			end
          		
        	end
    	end
	end
end

function process_tilesets (path, destination)
	for file in lfs.dir(path) do
    	if file ~= "." and file ~= ".." then

       		local f = path..'/'..file

          	local attr = lfs.attributes (f)
          	assert (type(attr) == "table")
          	if attr.mode == "directory" then
            	process_tilesets (f, destination)
          	else

          		if string.find(f, destination) then
          			local extension = f:match("^.+(%..+)$")
          			if extension == ".dat" then
          				solarus_resizer.resize_tileset(f, f, ratio)
      				end
      			end
          		
        	end
    	end
	end
end

function process_sprites (path, destination)
	for file in lfs.dir(path) do
    	if file ~= "." and file ~= ".." then

       		local f = path..'/'..file

          	local attr = lfs.attributes (f)
          	assert (type(attr) == "table")
          	if attr.mode == "directory" then
            	process_sprites (f, destination)
          	else
          		if string.find(f, destination) then
          			local extension = f:match("^.+(%..+)$")
          			if extension == ".dat" then
          				solarus_resizer.resize_sprite(f, f, ratio)
      				end
      			end
          		
        	end
    	end
	end
end

copy_folder(origin, origin, target_directory)
process_maps(target_directory, target_directory .. "/" .. "maps")
process_tilesets(target_directory, target_directory .. "/" .. "tilesets")
process_sprites(target_directory, target_directory .. "/" .. "sprites")

