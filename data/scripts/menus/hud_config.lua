-- Defines the elements to put in the HUD
-- and their position on the game screen.

-- You can edit this file to add, remove or move some elements of the HUD.

-- Each HUD element script must provide a method new()
-- that creates the element as a menu.
-- See for example scripts/hud/hearts.

-- Negative x or y coordinates mean to measure from the right or bottom
-- of the screen, respectively.

local game_scale = 5

local hud_config = {
  
  -- Hearts meter.
  {
    menu_script = "scripts/menus/hearts",
    x = -89 * game_scale,
    y = 8 * game_scale,
  }

}

return hud_config
