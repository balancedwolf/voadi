-- Pans across Rachel on her boat
require("scripts/utils")

local game_scale = 5

local scene = {}

-- Called when the scene is created
function scene:on_started()
  self.main_surface = sol.surface.create(512 * game_scale, 256 * game_scale)
  -- Load graphics
  self.boat_gfx  = sol.sprite.create("menus/intro/boat")
  self.sky_gfx   = sol.surface.create(512 * game_scale, 95 * game_scale)
  self.sky_gfx:fill_color({160, 240, 240})
  self.cloud_lg  = sol.sprite.create("menus/intro/cloud_large")
  self.cloud_sm  = sol.sprite.create("menus/intro/cloud_small")
  self.shine_gfx = sol.sprite.create("menus/intro/ocean_shine")

  do -- Pan the scene
    local m = sol.movement.create("target")
    -- m:set_target(-256 * game_scale, -112 * game_scale) -- perflectly diagonal
    m:set_target(-120 * game_scale, -88 * game_scale)
    m:set_speed(36 * game_scale)
    m:start(self.main_surface, function()
      sol.menu.stop(scene)
    end)
  end

  do -- Move big cloud
    local m = translate("x", -232 * game_scale)
    m:set_speed(70 * game_scale)
    m:start(self.cloud_lg)
  end

  do -- Move small cloud
    local m = translate("x", -232)
    m:set_speed(70 * game_scale)
    m:start(self.cloud_sm)
  end
end

-- Called every frame
function scene:on_draw(dst_surface)
  local main_surface = self.main_surface

  main_surface:fill_color({32, 208, 240})
  self.sky_gfx:draw(main_surface)
  self.cloud_lg:draw(main_surface, 146 * game_scale, 10 * game_scale)
  self.cloud_sm:draw(main_surface, 242 * game_scale, 57 * game_scale)
  self.boat_gfx:draw(main_surface, 184 * game_scale, 32 * game_scale)
  self.shine_gfx:draw(main_surface, 144 * game_scale, 104 * game_scale)
  self.shine_gfx:draw(main_surface, 344 * game_scale, 192 * game_scale)

  main_surface:draw(dst_surface)
end

return scene
