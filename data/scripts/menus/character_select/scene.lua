-- "OK" appears on the screen and shakes. Animated BG.
require("scripts/utils")
local game_manager = require("scripts/game_manager")


local scene = {}

local game_scale = 5

-- Called when the scene starts
function scene:on_started()
  self.dot_gfx       = sol.sprite.create("menus/intro/dot")
  self.dot_surface1   = sol.surface.create(520 * game_scale, 288 * game_scale)
  self.dot_surface2   = sol.surface.create(520 * game_scale, 288 * game_scale)
  
  self.selector_gfx = sol.sprite.create("menus/selector")

  self.selector_gfx:set_animation("blink")

  -- Draw the dots
  function draw_dots()
    local row = 0
    local col = 0
    while true do
      local x = col * 40 
      local y = row * 40 
      if x >= 512 then
        row = row + 1
        col = 0
      end
      if y >= 288 then
        return
      end
      x = col * 40
      y = row * 40
      self.dot_gfx:draw(self.dot_surface1, x * game_scale, y * game_scale)
      self.dot_gfx:draw(self.dot_surface2, x * game_scale, y * game_scale)
      col = col + 1
    end
  end

  draw_dots()

  function move_dots1()
    do -- Move dots
      local m = sol.movement.create("straight")
      --m:set_angle(math.pi * 1.25) -- 225 degrees
      m:set_speed(70 * game_scale)
      m:set_max_distance(520 * game_scale)
      m.on_finished = function() self.dot_surface1:set_xy(0, 0) move_dots1() end
      m:start(self.dot_surface1)
    end
  end

  function move_dots2()
    do -- Move dots
      local m = sol.movement.create("straight")
     --m:set_angle(math.pi * 1.25) -- 225 degrees
      m:set_speed(70 * game_scale)
      m:set_max_distance(520 * game_scale)
      m.on_finished = function() self.dot_surface2:set_xy(0, 0) move_dots2() end
      m:start(self.dot_surface2)
    end
  end

  move_dots1()
  move_dots2()

  self.selector_position_x = 64
  self.selection = "rachel"

  self.rachel_name_surface = sol.text_surface.create{
    horizontal_alignment = "left",
    vertical_alignment = "top",
    font = "Poco",
    color = {0, 0, 0},
    font_size = 10 * game_scale,
  }

  self.regan_name_surface = sol.text_surface.create{
    horizontal_alignment = "left",
    vertical_alignment = "top",
    font = "Poco",
    color = {0, 0, 0},
    font_size = 10 * game_scale,
  }

  self.rachel_name_surface:set_text("Rachel")
  self.regan_name_surface:set_text("Regan")


  self.rachel_photo_sprite = sol.sprite.create("menus/dialog_photos")
  self.regan_photo_sprite = sol.sprite.create("menus/dialog_photos")

  self.rachel_photo_sprite:set_animation("rachel")
  self.regan_photo_sprite:set_animation("regan")

  

end

function scene:on_command_pressed(command)
  if command == "left" then
    if self.selection == "rachel" then
      self.selector_position_x = 160
      self.selection = "regan"
    else
      self.selector_position_x = 64
      self.selection = "rachel"
    end
  elseif command == "right" then
    if self.selection == "regan" then
      self.selector_position_x = 64
      self.selection = "rachel"
    else
      self.selector_position_x = 160 
      self.selection = "regan"
    end
  elseif command == "action" then
    sol.menu.stop_all(sol.main)
    game_manager:new_game()
  end

  return true
end


-- Called every frame
function scene:on_draw(dst_surface)
  dst_surface:fill_color({32, 208, 240})
  self.dot_surface1:draw(dst_surface, 0, -144 * game_scale)
  self.dot_surface2:draw(dst_surface, -520 * game_scale, -144 * game_scale)
  self.rachel_name_surface:draw(dst_surface, (64 + 4) * game_scale, 32 * game_scale)
  self.regan_name_surface:draw(dst_surface, (160 + 4) * game_scale, 32 * game_scale)
  self.rachel_photo_sprite:draw(dst_surface, (64) * game_scale, 56 * game_scale)
  self.regan_photo_sprite:draw(dst_surface, (160) * game_scale, 56 * game_scale)
  self.selector_gfx:draw(dst_surface, self.selector_position_x * game_scale, 56 * game_scale) 
end

return scene
