local all = require("scripts/meta/all")
local destination_meta = sol.main.get_metatable("destination")

function destination_meta:on_created()
  self:set_size(80, 80)
end

